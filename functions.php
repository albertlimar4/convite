<?php

function getAnoAtual()
{
    return date("Y");
}

function post($key)
{
    return str_replace("'", "", $_POST[$key]);
}

function get($key)
{
    return str_replace("'", "", $_GET[$key]);
}
