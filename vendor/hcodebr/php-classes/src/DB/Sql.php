<?php

namespace Hcode\DB;

use PDO;

class Sql {

    const HOSTNAME_ON = "162.241.33.176";
    const USERNAME_ON = "acads506_albert";
    const PASSWORD_ON = "F@dire304";
    const DBNAME_ON = "acads506_ead";

    const HOSTNAME_LC = "127.0.0.1";
    const USERNAME_LC = "root";
    const PASSWORD_LC = "";
    const DBNAME_LC = "ead";

    private $conn;

    public function __construct()
    {
        $url = $_SERVER['HTTP_HOST'];

        if ($url == 'convite.naturologiaclinica.org'){

            error_reporting(0);

            $this->conn = new PDO(
                "mysql:dbname=".Sql::DBNAME_ON.";host=".Sql::HOSTNAME_ON.";charset=utf8",
                Sql::USERNAME_ON,
                Sql::PASSWORD_ON
            );

        }
        elseif ($url == 'acadsoftconvite.com.br'){

            $this->conn = new PDO(
                "mysql:dbname=".Sql::DBNAME_LC.";host=".Sql::HOSTNAME_LC.";charset=utf8",
                Sql::USERNAME_LC,
                Sql::PASSWORD_LC
            );

        }

    }

    private function setParams($statement, $parameters = array())
    {

        foreach ($parameters as $key => $value) {

            $this->bindParam($statement, $key, $value);

        }

    }

    private function bindParam($statement, $key, $value)
    {

        $statement->bindParam($key, $value);

    }

    public function query($rawQuery, $params = array())
    {

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();

    }

    public function select($rawQuery, $params = array()):array
    {

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function insert($rawQuery, $vParam = array())
    {

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $vParam);

        if ($stmt->execute())
            return $this->conn->lastInsertId();
        else
            return $stmt->errorInfo();

    }

}