<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Auxiliar extends Model
{

    public static function rgEmissor() {

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_rg_emissor");

    }

    public static function listEscolaridade(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_escolaridade ORDER BY id");

    }

    public static function listEstadoCivil(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_estado_civil ORDER BY id");

    }

    public static function listPais(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_pais");

    }

    public static function listEstado(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_estado");

    }

    public static function listEstadoPorPais($sNomePais){

        $sql = new Sql();

        if ($sNomePais != "Brasil"){
            $sQuery = "SELECT * FROM aux_estado WHERE ISNULL(pais)";
        } else {
            $sQuery = "SELECT * FROM aux_estado INNER JOIN aux_pais ON cod_pais = pais WHERE pai_nome = :sNomePais";
        }

        return $sql->select($sQuery, [
            "sNomePais"=>$sNomePais
        ]);

    }

    public static function listCidade(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_cidade ORDER BY nome");

    }

    public static function listCidadePorEstado($sSiglaEstado){

        $sql = new Sql();

        $sQuery = "SELECT * FROM aux_cidade INNER JOIN aux_estado ON estado = cod_estado WHERE est_sigla = :sSiglaEstado";

        return $sql->select($sQuery, [
            "sSiglaEstado"=>$sSiglaEstado
        ]);

    }

    public static function listProfissao(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_profissao ORDER BY nome");

    }

    public static function listTelefoneTipo(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_telefone_tipo ORDER BY tipo");

    }



}