<?php

namespace Hcode\Model;

use Exception;
use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getEviNome()
 * @method getEviTelefone()
 * @method getEviProfissao()
 * @method getEviEmail()
 * @method getEviCidade()
 */
class Evento extends Model {

    const SESSION = "User";
    const SECRET = "HcodePhp7_Secret";
    const ERROR_REGISTER = "UserErrorRegister";

    public static function listInscritos(): array
    {

        $sql = new Sql();

        return $sql->select("SELECT * FROM ev_inscricao a INNER JOIN ev_inscricao_evento b ON a.id = b.ev_inscricao_id WHERE b.ev_evento_id = 1 ORDER BY a.nome");

    }

    public static function setErrorRegister($msg){

        if (!isset($_SESSION))session_start();

        $_SESSION[Evento::ERROR_REGISTER] = $msg;

    }

    public static function listProgramacao($nIdEvento): array
    {

        $sql = new Sql();

        return $sql->select("SELECT pro_descricao, DATE_FORMAT(pro_inicio, '%H:%i') as inicio FROM ev_evento_programacao WHERE eve_id = :nIdEvento ORDER BY inicio", [
            ":nIdEvento"=>$nIdEvento
        ]);

    }

    public static function getErrorRegister(){

        if (!isset($_SESSION))session_start();

        $msg = (isset($_SESSION[Evento::ERROR_REGISTER]) && $_SESSION[Evento::ERROR_REGISTER]) ? $_SESSION[Evento::ERROR_REGISTER] : '';

        Evento::clearErrorRegister();

        return $msg;

    }

    public static function clearErrorRegister(){

        $_SESSION[Evento::ERROR_REGISTER] = NULL;

    }

    /**
     * @throws Exception
     */
    public function saveInscricao(): array
    {

        $sNome = trim(mb_strtoupper($this->getEviNome(), 'utf-8'));
        $sEmail = trim(strtolower($this->getEviEmail()));
        $sTelefone = trim(strtolower($this->getEviTelefone()));

        $sql = new Sql();

        if (self::consultaEmail($sEmail))
            return ["success"=>0,"sMsg"=>"Email já cadastrado anteriormente!","sClass"=>"alert alert-danger"];

        $oInscricao = $sql->insert("Insert Into ev_evento_inscricao (evi_nome, evi_telefone, evi_email, evi_profissao, evi_cidade, eve_id) Values (:nome, :telefone, :email, :profissao, :cidade, :evento)", array(
            ":nome"=>$sNome,
            ":telefone"=>$sTelefone,
            ":email"=>$sEmail,
            ":profissao"=>$this->getEviProfissao(),
            ":cidade"=>$this->getEviCidade(),
            ":evento"=>1
        ));

        if (!$oInscricao)
            return ["success"=>0,"sMsg"=>"Erro interno, tente novamente mais tarde!","sClass"=>"alert alert-danger"];


        return ["success"=>1,"sMsg"=>"Inscrição realizada com sucesso!","sClass"=>"success"];

    }

    public static function consultaEmail($sUseEmail): bool
    {

        $sql = new Sql();

        $oAluno = $sql->select("Select * From ev_evento_inscricao Where evi_email = :evi_email", [
            ":evi_email"=>$sUseEmail
        ]);

        return (bool)$oAluno;

    }

}