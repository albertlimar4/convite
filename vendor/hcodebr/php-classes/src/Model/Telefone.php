<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Telefone extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM aux_telefone_tipo ORDER BY tipo");

    }

    public function save(){

        $sql = new Sql();

        $nTelId = $this->gettelid();
        $nDdi = $this->getddi();
        $nDdd = $this->getddd();
        $sTelefone = $this->gettelefone();

        $results = $sql->select("CALL sp_telefone_save (:ddi, :ddd, :numero, :tipo, :pessoa)", array(
            ":ddi"=>$nDdi,
            ":ddd"=>$nDdd,
            ":numero"=>$sTelefone,
            ":pessoa"=>$oAluno['pessoaid']
        ));

        $this->setData($results[0]);

    }

    public function get($idcategoria){

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM telefone WHERE id = :id", array(
            ":id"=>$idcategoria
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        $nTelId = $this->gettelid();
        $nDdi = $this->getddi();
        $nDdd = $this->getddd();
        $sTelefone = $this->gettelefone();

        $results = $sql->select("CALL sp_telefone_update (:id, :rua, :numero, :complemento, :bairro, :cidade, :estado, :cep)", array(
            ":id"=>$nTelId,
            ":ddd"=>$nDdd,
            ":numero"=>$sTelefone
        ));

        $this->setData($results[0]);

    }

    public function delete(){

        $sql = new Sql();

        $sql->select("DELETE FROM telefone WHERE id = :id", array(
            ":id"=>$this->getid()
        ));

    }

}