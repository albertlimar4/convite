<?php

require_once("vendor/autoload.php");

use Slim\Slim;
use Hcode\Page;
use Hcode\Model\Evento;

$app = new Slim();

$app->config('debug', true);

require_once "functions.php";

$app->get('/', function () {

    $voProgramacao = Evento::listProgramacao(2);

    $page = new Page();

    $page->setTpl("simposio", [
        "voProgramacao" => $voProgramacao
    ]);

});

$app->get('/sucesso', function () {

    $page = new Page();

    $page->setTpl("sucesso");

});

$app->get('/inscricao', function () {

    if (isset($_GET['EviNome'])){
        $voInscricao = new Evento();

        $voInscricao->setData($_GET);

        echo json_encode($voInscricao->saveInscricao());
        exit();
    }

    $page = new Page([
        "header" => false,
        "footer" => false
    ]);

    $page->setTpl("inscricao");

});

$app->post('/inscricao', function () {

//    var_dump($_GET);

    $voInscricao = new Evento();

    $voInscricao->setData($_GET);

    echo json_encode($voInscricao->saveInscricao());

});

$app->run();