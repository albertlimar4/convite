<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Saúde e Qualidade de Vida</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <script type="text/javascript">
        $(function() {
          $('a').bind('click',function(event){
            var $anchor = $(this);
            $('html, body').stop().animate({scrollTop: $($anchor.attr('href')).offset().top}, 1000,'swing');
          });
        });
    </script>

    <header class="main_header">
        <div class="container main_header_content">
            <div class="main_header_content_headline">
                <h1>ENCONTRO:<br>
                Como Evitar e Vencer Qualquer Doença Física?</h1>
                <p style="font-size:110%">Venha participar de inédita e única apresentação de um dos maiores especialista em Medicina Natural do Brasil</p>
            </div>
            <div class="main_header_content_cta">
                <a href="#inscricao" class=""><span class="button_cta transition radius main_header_content_cta_btn">FAÇA SUA INSCRIÇÃO</span></a>
            </div>
        </div>
    </header>

    <main>
        <section class="bullet_points">
            <header class="bullet_points_header">
                <h1>Por Que Participar do Encontro?</h1>
            </header>
            <div class="container bullet_points_content">
                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/terapeuta.png" alt="Como as Terapias Naturais Podem Melhorar Sua Saúde?" title="Como as Terapias Naturais Podem Melhorar Sua Saúde?"/>
                    </div>
                    <div class="bullet">
                        <h2>TERAPIAS - Como as Terapias Naturais Podem Melhorar Sua Saúde?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/alimento.png" alt="Como a Alimentação Afeta Nossa Vida?" title="Como a Alimentação Afeta Nossa Vida?"/>
                    </div>
                    <div class="bullet">
                        <h2>ALIMENTAÇÃO - Como a Alimentação Afeta Nossa Vida?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/hidratacao.png" alt="Como a Água Elimina Desidratação Celular?" title="Como a Água Elimina Desidratação Celular?"/>
                    </div>
                    <div class="bullet">
                        <h2>HIDRATAÇÃO - Como a Água Elimina Desidratação Celular?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/coach.png" alt="Como Evitar e Vencer Qualquer Doença Física?" title="Como Evitar e Vencer Qualquer Doença Física?"/>
                    </div>
                    <div class="bullet">
                        <h2>TREINAMENTOS - Como Evitar e Vencer Qualquer Doença Física?</h2>
                    </div>
                </article>

            </div>
        </section>

        <article class="optin" id="inscricao">
            <div class="container">
                <div class="optin_content">
                    <header class="optin_content_header">
                        <h1>Faça seu cadastro para Estar Conosco no Encontro!</h1>
                    </header>

                    <div class="main_cta">
                        <a href="http://naturologia.professorjean.com/inscricao/" class="" data-toggle="modal" data-target="#modalForm"><span class="button_cta radius transition">Increva-se Agora!</span></a>
                    </div>
                </div>
            </div>
        </article>

        <article class="author">
            <div class="container">
                <div class="author_content">
                    <div class="author_content_img">
                        <img class="rounded" src="images/author.png" alt="Jean Alves Cabral - Naturologista Clínico" title="Jean Alves Cabral - Naturologista Clínico"/>
                    </div>
                    <header class="author_content_header">
                        <h1><b>Jean Alves Cabral</b> - Naturologista Clínico.</h1>
                        <p>Com mais de 20 anos de formação na área de Naturologia Clínica, Especialista em Iridossomatologia, Doutor em Naturopatia Científica. Coordenador Geral.</p>
                        <p><b>Faça seu cadastro e comece agora!</b></p>
                    </header>
                </div>
            </div>
        </article>

        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="Formulário de Inscrição">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Inscrição</h4>
                    </div>
                    <div class="modal-body">

                        <form class="form form-horizontal" method="post" action="parabens.php">

                            <div class="form-group">

                                <label for="nome" class="control-label col-sm-1">Nome*</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="nome" id="nome">
                                </div>

                                <label for="email" class="control-label col-sm-1">Email*</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="email" id="email">
                                </div>

                                <label for="fone" class="control-label col-sm-1">Telefone*</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="fone" id="fone">
                                </div>

                                <label for="profissao" class="control-label col-sm-1">Profissão*</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="profissao" id="profissao">
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-offset-1 col-sm-12">
                                    <button type="submit" class="btn btn-success">Enviar</button>
                                </div>

                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>

    </main>

    <footer class="main_footer">
        <div>&COPY; www.professorjean.com - todos os direitos reservados!</div>
        <!--             <div>
        <a href="#" title="">Saiba mais</a> |
        <a href="#" title="">Termos de uso</a>
        </div> -->
    </footer>
</body>
</html>