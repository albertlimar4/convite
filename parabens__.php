<?php
ini_set("display_errors", 0);
error_reporting(0);

date_default_timezone_set('America/Sao_Paulo');

$host = '192.185.216.121';
$user = 'acads506_albert';
$pass = 'F@dire304';
$db = 'acads506_ead';

//$host = '127.0.0.1';
//$user = 'root';
//$pass = '';
//$db = 'db_ead';


$con = mysqli_connect($host, $user, $pass);
mysqli_set_charset($con, 'utf8');

if(!($con)){
    echo "Não foi possível estabelecer a conexão";
}

$teste = mysqli_select_db($con, $db);

if(!($teste)){
    echo "Não foi possível estabelecer conexão com o banco de dados";
}

$nome = trim($_POST['nome']);
$email = trim($_POST['email']);
$fone = trim($_POST['fone']);
$profissao = trim($_POST['profissao']);

$q_pessoa = mysqli_query($con, "INSERT INTO ev_inscricao (nome, email, telefone, profissao) VALUES ('$nome', '$email', '$fone', '$profissao')");

$id = mysqli_insert_id($con);

$q_inscricao = mysqli_query($con, "INSERT INTO ev_inscricao_evento (ev_evento_id, ev_inscricao_id) VALUES (1, '$id')");

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Saúde e Qualidade de Vida</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <header class="main_header">
        <div class="container main_header_content">
            <div class="main_header_content_headline">
                <p>CURSO DE FORMAÇÃO EM NATUROLOGIA CLÍNICA</p>
                <h1>Parabéns!</h1>
                <p>Sua inscrição foi realizada com sucesso!</p>
            </div>
            <div class="main_header_content_cta">
                <iframe src="https://player.vimeo.com/video/275663838?autoplay=1" width="100%" height="250" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </header>

    <main>
        <section class="bullet_points">
            <header class="bullet_points_header">
                <h1>Por Que Participar do Encontro?</h1>
            </header>
            <div class="container bullet_points_content">
                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/terapeuta.png" alt="Como as Terapias Naturais Podem Melhorar Sua Saúde?" title="Como as Terapias Naturais Podem Melhorar Sua Saúde?"/>
                    </div>
                    <div class="bullet">
                        <h2>TERAPIAS - Como as Terapias Naturais Podem Melhorar Sua Saúde?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/alimento.png" alt="Como a Alimentação Afeta Nossa Vida?" title="Como a Alimentação Afeta Nossa Vida?"/>
                    </div>
                    <div class="bullet">
                        <h2>ALIMENTAÇÃO - Como a Alimentação Afeta Nossa Vida?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/hidratacao.png" alt="Como a Água Elimina Desidratação Celular?" title="Como a Água Elimina Desidratação Celular?"/>
                    </div>
                    <div class="bullet">
                        <h2>HIDRATAÇÃO - Como a Água Elimina Desidratação Celular?</h2>
                    </div>
                </article>

                <article class="bullet_points_content_item">
                    <div class="img">
                        <img src="images/coach.png" alt="Como Evitar e Vencer Qualquer Doença Física?" title="Como Evitar e Vencer Qualquer Doença Física?"/>
                    </div>
                    <div class="bullet">
                        <h2>TREINAMENTOS - Como Evitar e Vencer Qualquer Doença Física?</h2>
                    </div>
                </article>

            </div>
        </section>

        <section class="bullet_points optin">
            <header class="depoimentos_header">
                <h1>Testemunhos</h1>
            </header>
            <div class="container bullet_points_content">
                <article class="bullet_points_content_item">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/RYEIhZ2ke6o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </article>

                <article class="bullet_points_content_item">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/-F5UeoegHqs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </article>

                <article class="bullet_points_content_item">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/WbRNpgax_30" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </article>

                <article class="bullet_points_content_item">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/7I_JvIO9v0g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </article>

                <article class="bullet_points_content_item">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/tHhRmHHFNl8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </article>
            </div>
        </section>

        <article class="author">
            <div class="container">
                <div class="author_content">
                    <div class="author_content_img">
                        <img class="rounded" src="images/author.png" alt="Robson V. Leite - CEO/Founder UpInside, criado do programa mentor" title="Robson V. Leite - CEO/Founder UpInside, criado do programa mentor"/>
                    </div>
                    <header class="author_content_header">
                        <h1><b>Jean Alves Cabral</b> - Naturologista Clínico.</h1>
                        <p>Com mais de 20 anos de formação na área de Naturologia Clínica, Especialista em Iridossomatologia, Doutor em Naturopatia Científica. Coordenador Geral.</p>
                        <p><b>Faça seu cadastro e comece agora!</b></p>
                    </header>
                </div>
            </div>
        </article>
    </main>

    <footer class="main_footer">
        <div>&COPY; www.professorjean.com - todos os direitos reservados!</div>
    </footer>
</body>
</html>